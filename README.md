## ConcentrationGame V1

### Features
* User can choose one of 3 difficulty levels
* User can view game history for each difficulty level
* User can quit the game
* User can restart the game
* User has to save the new score at the end of the game