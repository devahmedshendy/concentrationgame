//
//  GameLevel.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/10/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation

enum GameLevel {
    case Low
    case Intermediate
    case Hard
    
    var cardsPerRow: Int {
        return 5
    }
    
    var numberOfCards: Int {
        switch self {
        case .Low: return cardsPerRow * 2
        case .Intermediate: return cardsPerRow * 4
        case .Hard: return cardsPerRow * 6
        }
    }
    
    var numberOfPairsOfCards: Int {
        return numberOfCards / 2
    }
}
