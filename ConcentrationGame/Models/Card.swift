//
//  Card.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/12/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation

struct Card: Hashable {
    
    //MARK: Properties
    
    var isFaceUp = false
    var isMatched = false
    private var identifier: Int
    
    //MARK: Initializations
    
    init() {
        identifier = Card.getUniqueIdentifier()
    }
    
    //MARK: Static Properties & Helper Methods
    
    private static var identifierFactory = 0
    
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    static func resetIdentifierFactory() {
        identifierFactory = 0
    }
    
    //MARK: Helper Methods
    
    mutating func faceItDown(matched: Bool) {
        isFaceUp = false
        isMatched = matched
    }
    
    //MARK: Protocol Implementations
    
    var hashValue: Int {
        return identifier
    }
    
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
