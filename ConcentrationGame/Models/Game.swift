//
//  GameBoard.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/10/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation

class Game {
    
    //MARK: Stored Properties
    
    private(set) var level: GameLevel!
    private(set) var cards = [Card]()
    private(set) var firstFacedUpCardIndex: Int?
    private(set) var secondFacedUpCardIndex: Int?
    
    //MARK: Computed Properties
    
    var twoCardsAreFaceUp: Bool {
        return firstFacedUpCardIndex != nil && secondFacedUpCardIndex != nil
    }
    
    var firstFacedUpCard: Card? {
        return firstFacedUpCardIndex == nil ? nil : cards[firstFacedUpCardIndex!]
    }
    
    var secondFacedUpCard: Card? {
        return secondFacedUpCardIndex == nil ? nil : cards[secondFacedUpCardIndex!]
    }
    
    var isDone: Bool {
        return cards.first(where: { !$0.isMatched }) == nil ? true : false
    }
    
    var isFlipAvailable: Bool {
        return firstFacedUpCardIndex == nil || secondFacedUpCardIndex == nil
    }
    
    //MARK: Initializations
    
    init(gameLevel: GameLevel) {
        Card.resetIdentifierFactory()
        level = gameLevel
        
        for _ in 1...level.numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }
        
        shuffleCards()
    }
    
    //MARK: Helper Methods
    
    func chooseCard(_ index: Int) {
        cards[index].isFaceUp = true
        
        if firstFacedUpCardIndex == nil {
            firstFacedUpCardIndex = index
            
        } else if secondFacedUpCardIndex == nil {
            secondFacedUpCardIndex = index
        }
    }
    
    func restart() {
        Card.resetIdentifierFactory()
        cards.removeAll()
        firstFacedUpCardIndex = nil
        secondFacedUpCardIndex = nil
        
        for _ in 1...level.numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }
        
        shuffleCards()
    }
    
    func faceFlippedCardsDown(bothMatched: Bool) {
        guard let firstIndex = firstFacedUpCardIndex, let secondIndex = secondFacedUpCardIndex else {
            fatalError("Trying to face down flipped cards when there is only-one or none flipped cards")
        }
        
        cards[firstIndex].faceItDown(matched: bothMatched)
        cards[secondIndex].faceItDown(matched: bothMatched)
        
        firstFacedUpCardIndex = nil
        secondFacedUpCardIndex = nil
    }
    
    private func shuffleCards() {
        for index in cards.indices {
            let randomIndex = cards.count.randomButNotEqual(to: index)
            cards.swapAt(index, randomIndex)
        }
    }
}

extension Int {
    var arc4random: Int {
        if self > 0 {
            return Int(arc4random_uniform(UInt32(self)))
        } else if self < 0 {
            return -Int(arc4random_uniform(UInt32(self)))
        } else {
            return 0
        }
    }
    
    func randomButNotEqual(to number: Int) -> Int {
        var random = self.arc4random

        while random == number {
            random = self.arc4random
        }
        
        return random
    }
}

extension Collection {
    var oneAndOnly: Element? {
        return count == 1 ? first : nil
    }
}
