//
//  GameScore.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/25/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation

struct GameScore: Hashable {
    let name: String
    let score: Int
    let gameDate: Date
}
