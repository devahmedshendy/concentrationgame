//
//  SaveGameScoreViewController.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/25/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class SaveGameScoreViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: Properties
    
    var gameLevel: GameLevel!
    var score: Int!
    var gameDate: Date!
    
    //MARK: Outlets
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var scoreNameTextField: UITextField!
    
    //MARK: App LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureSaveButton()
        configureNameTextField()
    }
    
    //MARK: Outlet Actions
    
    @IBAction func saveGameScore(_ sender: UIButton) {
        saveGameScore()
    }
    
    //MARK: TextField Delegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string)
        
        disableSaveButton(isTextEmpty: newText.isEmpty)
        
        return isStringBackspaceOrAlphabetOrNumber(string)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text == "" {
            return false
        }
        
        saveGameScore()
        return true
    }
    
    //MARK: Helper Methods
        
    private func configureSaveButton() {
        saveButton.isEnabled = false
        saveButton.backgroundColor = saveButton.backgroundColor?.withAlphaComponent(0.5)
    }
    
    private func configureNameTextField() {
        scoreNameTextField.returnKeyType = .done
        scoreNameTextField.becomeFirstResponder()
        scoreNameTextField.delegate = self
    }
    
    private func disableSaveButton(isTextEmpty: Bool) {
        saveButton.isEnabled = !isTextEmpty
        saveButton.backgroundColor = isTextEmpty ?
            saveButton.backgroundColor?.withAlphaComponent(0.5) :
            saveButton.backgroundColor?.withAlphaComponent(1)
    }
    
    private func doesStringContainWhitespaceOrTab(_ string: String) -> Bool {
        return !string.contains(" ") || !string.contains("\t")
    }
    
    private func isStringBackspaceOrAlphabetOrNumber(_ char: String) -> Bool {
        let alphabets = "abcdefghijklmnopqrstuvwxyz"
        let numbers = "0123456789"
        return char.isEmpty || alphabets.contains(char.lowercased()) || numbers.contains(char)
    }
    
    private func saveGameScore() {
        if GameHistoryModelController.isGameScoreUnique(scoreNameTextField.text!, level: gameLevel) {
            addScoreThenReturnBackToMainVC()
        } else {
            displayAlertThatScoreNameIsAlreadyUsed()
        }
    }
    
    private func addScoreThenReturnBackToMainVC() {
        let gameScore = GameScore.init(name: scoreNameTextField.text!, score: score, gameDate: gameDate)

        GameHistoryModelController.addScore(gameScore: gameScore, level: gameLevel)
        view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    private func displayAlertThatScoreNameIsAlreadyUsed() {
        let alertController = UIAlertController(title: "⚠️", message: "The name you provided is already used before .. please type another one", preferredStyle:  .alert)
        
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
