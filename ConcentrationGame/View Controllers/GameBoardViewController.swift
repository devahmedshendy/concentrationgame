//
//  ViewController.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/10/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class GameBoardViewController: UIViewController {
    
    //MARK: Properties
    
    var game: Game!
    private let emojiChoices = "😈👻🎃🤖👁🐵🦉🐞🐙🐟🐚🌻🌍🍎⚽️🎧🎲🖨⏰🛡💖❤️🇧🇪🇯🇵🔥🍇🥚🥛⚾️🎾"
    private var cardEmojiMap = [Card : String]()
    
    //MARK: Computed Properties
    
    private var flipCount = 0 {
        didSet {
            flipCountLabel.text = "Flips: \(flipCount)"
        }
    }
    
    //MARK: Outlets
    
    @IBOutlet var cardsBoardStack: UIStackView!
    @IBOutlet var gameActionButtons: [UIButton]!
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var flipCountLabel: UILabel!
    
    //MARK: App Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundActionButtons()
        roundDefaultCardButtons()
        setupCardsBoardStackForAdvancedLevels()
        buildCardEmojiMap()
    }
    
    //MARK: Outlet Actions

    @IBAction func touchCard(_ sender: UIButton) {
        let cardIndex = guardIfTouchedCardIsNotOfCardButtonsCollection(sender)
        handleTouchedCardWhenFaceDownAndNotMatched(cardIndex: cardIndex)
    }
    
    @IBAction func restartGame(_ sender: UIButton) {
        flipCount = 0
        game.restart()
        buildCardEmojiMap()
        resetCardButtonsStatus()
    }
    
    @IBAction func quitGame(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Segue Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let saveGameScoreVC = segue.destination as? SaveGameScoreViewController {
            saveGameScoreVC.gameLevel = game.level
            saveGameScoreVC.score = flipCount
            saveGameScoreVC.gameDate = Date()
        }
    }
    
    //MARK: Helper Methods
    
    private func guardIfTouchedCardIsNotOfCardButtonsCollection(_ button: UIButton) -> Int {
        guard let index = cardButtons.index(of: button) else {
            fatalError("The touched card button isn't available in the cards outlet collection")
        }
        
        return index
    }
    
    private func handleTouchedCardWhenFaceDownAndNotMatched(cardIndex: Int) {
        let touchedCard = game.cards[cardIndex]
        
        if !touchedCard.isFaceUp && !touchedCard.isMatched && game.isFlipAvailable {
            flipCount += 1
            game.chooseCard(cardIndex)
            setCardButtonFaceUp(cardIndex: cardIndex)
            
            handleTwoCardsAreFaceUp()
        }
    }
    
    private func handleTwoCardsAreFaceUp() {
        if game.twoCardsAreFaceUp {
            let firstCardIndex = game.firstFacedUpCardIndex!
            let secondCardIndex = game.secondFacedUpCardIndex!
            
            let firstCard = game.firstFacedUpCard!
            let secondCard = game.secondFacedUpCard!
            
            let matched = firstCard == secondCard
            
            delay(action: {
                self.game.faceFlippedCardsDown(bothMatched: matched)
                self.setCardButtonStatus(matched: matched, cardIndex: firstCardIndex)
                self.setCardButtonStatus(matched: matched, cardIndex: secondCardIndex)
                self.handleGameIsEnded()
                
            }, for: .milliseconds(500))
        }
    }
    
    private func handleGameIsEnded() {
        print("Game is Done: \(game.isDone)")
        
        if game.isDone {
            performSegue(withIdentifier: "saveGameScoreSegue", sender: self)
        }
    }
    
    private func setCardButtonFaceUp(cardIndex index: Int) {
        let card = self.game.cards[index]
        let cardButton = self.cardButtons[index]
        let cardEmoji = self.cardEmojiMap[card]
        
        cardButton.setTitle(cardEmoji, for: .normal)
        cardButton.backgroundColor = #colorLiteral(red: 0.2509803922, green: 0.6980392157, blue: 0.9176470588, alpha: 1)
    }
    
    private func setCardButtonStatus(matched: Bool, cardIndex: Int) {
        let cardButton = self.cardButtons[cardIndex]
        cardButton.setTitle("", for: .normal)
        cardButton.backgroundColor = matched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 0.3411764706, green: 0.7764705882, blue: 0.07843137255, alpha: 1)
    }
    
    private func resetCardButtonsStatus() {
        cardButtons.indices.forEach { cardIndex in
            setCardButtonStatus(matched: false, cardIndex: cardIndex)
        }
    }
    
    private func roundActionButtons() {
        gameActionButtons.forEach { button in
            button.layer.cornerRadius = button.frame.height / 2
        }
    }
    
    private func roundDefaultCardButtons() {
        cardButtons.forEach { $0.layer.cornerRadius = 5 }
    }
    
    private func setupCardsBoardStackForAdvancedLevels() {
        if game.level != .Low {
            let numberOfRemainingButtons = game.level.numberOfCards - cardButtons.count
            let numberOfRemainingCardButtonsRows = numberOfRemainingButtons / game.level.cardsPerRow
            
            for _ in 1...numberOfRemainingCardButtonsRows {
                let cardButtonsRow = createCardButtonsRow()
                
                for _ in 1...game.level.cardsPerRow {
                    let cardButton = createDefaultCardButton()
                    cardButtonsRow.addArrangedSubview(cardButton)
                }
                
                cardsBoardStack.addArrangedSubview(cardButtonsRow)
            }
        }
    }
    
    private func createCardButtonsRow() -> UIStackView {
        let cardButtonsRow = UIStackView()
        cardButtonsRow.axis = .horizontal
        cardButtonsRow.spacing = 10
        cardButtonsRow.alignment = .fill
        cardButtonsRow.distribution = .fillEqually
        
        return cardButtonsRow
    }
    
    private func createDefaultCardButton() -> UIButton {
        let cardButton = UIButton()
        cardButton.layer.cornerRadius = 5
        cardButton.frame.size = CGSize(width: 60.0, height: 60.0)
        cardButton.backgroundColor = #colorLiteral(red: 0.3400645256, green: 0.7751839757, blue: 0.07966206223, alpha: 1)
        cardButton.titleLabel?.font = cardButton.titleLabel?.font.withSize(30)
        
        cardButton.addTarget(self, action: #selector(touchCard(_:)), for: .touchUpInside)
        cardButtons.append(cardButton)
        
        return cardButton
    }
    
    private func buildCardEmojiMap() {
        var emojis = emojiChoices
        for index in game.cards.indices {
            let card = game.cards[index]
            let emojiIndex = emojis.index(emojis.startIndex, offsetBy: emojis.count.arc4random)
            
            cardEmojiMap[card] = String(emojis.remove(at: emojiIndex))
        }
    }
    
    private func delay(action execute: @escaping () -> Void, for period: DispatchTimeInterval) {
        DispatchQueue.main.asyncAfter(deadline: .now() + period, execute: execute)
    }
}

