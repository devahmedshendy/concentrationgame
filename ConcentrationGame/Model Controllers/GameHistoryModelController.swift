//
//  GameHistoryModelController.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/28/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation

class GameHistoryModelController {
    
    //MARK: Static Properties
    
    private static var lowLevelHistoryList = [GameScore] ()
    private static var intermediateLevelHistoryList = [GameScore] ()
    private static var hardLevelHistoryList = [GameScore] ()
    
    //MARK: Public Static Methods
    
    static func addScore(gameScore: GameScore, level: GameLevel) {
        switch level {
        case .Low:
            lowLevelHistoryList.append(gameScore)
        case .Intermediate:
            intermediateLevelHistoryList.append(gameScore)
        case .Hard:
            hardLevelHistoryList.append(gameScore)
        }
    }
    
    static func isGameScoreUnique(_ scoreName: String, level: GameLevel) -> Bool {
        switch level {
        case .Low:
            return noSuch(scoreName: scoreName, in: lowLevelHistoryList)
        case .Intermediate:
            return noSuch(scoreName: scoreName, in: intermediateLevelHistoryList)
        case .Hard:
            return noSuch(scoreName: scoreName, in: hardLevelHistoryList)
        }
    }
    
    static func getHistoryList(for level: GameLevel) -> [GameScore] {
        switch level {
        case .Low:
            return lowLevelHistoryList
        case .Intermediate:
            return intermediateLevelHistoryList
        case .Hard:
            return hardLevelHistoryList
        }
    }
    
    //MARK: Private Static Helper Methods
    
    private static func noSuch(scoreName: String, in historyList: [GameScore]) -> Bool {
        return !historyList.contains(where: { $0.name == scoreName })
    }
}
