//
//  ChooseLevelViewController.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/10/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet var gameHistoryButtons: [UIButton]!

    //MARK: App LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundGameHistoryButtons()
    }
    
    //MARK: Segue Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton {
            if let gameBoardVC = segue.destination as? GameBoardViewController {
                prepareGameBoardVC(gameBoardVC, button: button)
                
            } else if let gameHistoryVC = segue.destination as? GameHistoryViewController {
                prepareGameHistoryVC(gameHistoryVC, button: button)
            }
        }
    }
    
    //MARK: Helper Methods
    
    private func roundGameHistoryButtons() {
        gameHistoryButtons.forEach { button in
            button.layer.cornerRadius = button.frame.height / 2
        }
    }
    
    private func prepareGameBoardVC(_ gameBoardVC: GameBoardViewController, button: UIButton) {
        switch button.tag {
        case 0: gameBoardVC.game = Game(gameLevel: .Low)
        case 1: gameBoardVC.game = Game(gameLevel: .Intermediate)
        case 2: gameBoardVC.game = Game(gameLevel: .Hard)
            
        default:
            fatalError("Level Button of tag \(button.tag) is not supported in the Game")
        }
    }
    
    private func prepareGameHistoryVC(_ gameHistoryVC: GameHistoryViewController, button: UIButton) {
        switch button.tag {
        case 0: gameHistoryVC.gameLevel = .Low
        case 1: gameHistoryVC.gameLevel = .Intermediate
        case 2: gameHistoryVC.gameLevel = .Hard
            
        default:
            fatalError("Level History Button of tag \(button.tag) is not supported in the Game")
        }
    }
}
