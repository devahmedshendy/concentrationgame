//
//  GameHistoryViewController.swift
//  ConcentrationGame
//
//  Created by  Ahmed Shendy on 10/25/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class GameHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Properties
    var gameLevel: GameLevel!
    
    //MARK: Outlet Properties
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var gameHistoryTable: UITableView!
    
    //MARK: App Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        roundCloseButton()
        configureGameHistoryTableView()
        renameHeaderLabelWithGameLevel()
    }
    
    //MARK: Outlet Actions
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: TableView Delegate & DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GameHistoryModelController.getHistoryList(for: gameLevel).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let gameScore = GameHistoryModelController.getHistoryList(for: gameLevel)[indexPath.row]
        let gameScoreCell = tableView.dequeueReusableCell(withIdentifier: "gameScoreCell")!
        
        gameScoreCell.textLabel?.text = indexPath.row == 0 ? "\(gameScore.score)\t\t\(gameScore.name)\t #TOP" : "\(gameScore.score)\t\t\(gameScore.name)"
        gameScoreCell.detailTextLabel?.text = "\(formatDate(gameScore.gameDate))"
        
        return gameScoreCell
    }
    
    
    //MARK: App Lifecycle Helper Methods
    
    private func roundCloseButton() {
        closeButton.layer.cornerRadius = closeButton.frame.height / 2
    }
    
    private func configureGameHistoryTableView() {
        gameHistoryTable.delegate = self
        gameHistoryTable.dataSource = self
    }
    
    private func renameHeaderLabelWithGameLevel() {
        switch gameLevel! {
        case .Low: headerLabel.text = "Low - \(headerLabel.text!)"
        case .Intermediate: headerLabel.text = "Intermediate - \(headerLabel.text!)"
        case .Hard: headerLabel.text = "Hard - \(headerLabel.text!)"
        }
    }
    
    private func formatDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.setLocalizedDateFormatFromTemplate("MMMdyyyyh:mma")
        
        return dateFormatter.string(from: date)
    }
}
